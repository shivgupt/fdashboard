import axios from "axios";

import {
  DateString,
  DecimalString,
  TimestampString,
} from "../types";

export type PriceData = {
  ids: { [assetType: string]: string };
  [date: string]: {
    [assetType: string]: DecimalString;
  };
}

const fetchPrice = async (
  asset: string,
  timestamp: TimestampString,
): Promise<string> => {
  const emptyPriceData: PriceData = { ids: {} };

  const date = (timestamp.includes("T") ? timestamp.split("T")[0] : timestamp) as DateString;
  const coingeckoUrl = "https://api.coingecko.com/api/v3";

  const loadCache = () : PriceData => {
    try {
      let priceData = localStorage.getItem('priceData')
      if (priceData) return JSON.parse(priceData)
      return emptyPriceData;
    } catch (e) {
        return emptyPriceData;
      }
    };

  const saveCache = (priceData: PriceData): void => localStorage.setItem('priceData', JSON.stringify(priceData))

  const prices = loadCache() as PriceData;
  if (!prices[date]) {
    prices[date] = {};
  }

  if (!prices[date][asset]) {

    // get coin id
    if (!prices.ids[asset]) {
      console.log(`Fetching coin id for ${asset}..`);
      const coins = (await axios(`${coingeckoUrl}/coins/list`)).data;
      const coin = coins.find((coin: any) => coin.symbol.toLowerCase() === asset.toLowerCase());
      if (!coin || !coin.id) {
        throw new Error(`Asset ${asset} is not supported by coingecko`);
      }
      prices.ids[asset] = coin.id;
      //saveCache(prices);
    }
    const coinId = prices.ids[asset];

    // get coin price
    // https://api.coingecko.com/api/v3/coins/bitcoin/history?date=30-12-2017

    // DD-MM-YYYY
    const coingeckoDate = `${date.split("-")[2]}-${date.split("-")[1]}-${date.split("-")[0]}`;
    console.log(`Fetching price of ${asset} on ${coingeckoDate}..`);
    try {
      const response = (await axios(
        `${coingeckoUrl}/coins/${coinId}/history?date=${coingeckoDate}`,
      )).data;
      prices[date][asset] = response.market_data.current_price.usd.toString();
    } catch (e) {
      //throw new Error(`Couldn't get price, double check that ${asset} existed on ${coingeckoDate}`);
      //TODO: better way to cache when asset did not exist
      console.log(`Couldn't get price, double check that ${asset} existed on ${coingeckoDate}`)
      prices[date][asset] = '0';
    }
      saveCache(prices);
  }

  return prices[date][asset];
};

export const getPrice = async (asset: string, date: string): Promise<string> =>
  ["USD", "DAI", "SAI"].includes(asset)
    ? "1"
    : ["ETH", "WETH"].includes(asset)
    ? await fetchPrice("ETH", date)
    : asset.toUpperCase().startsWith("C")
    ? "0" // skip compound tokens for now
    : await fetchPrice(asset, date);
